package com.cv.timo;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.app.Activity;
import android.view.Gravity;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MainActivity extends Activity {
	ImageView kuva;
	int counter,last;
	Button back, forw, start, exit;

	// @Override
	// protected void onCreate(Bundle savedInstanceState) {
	// super.onCreate(savedInstanceState);
	// setContentView(R.layout.activity_main);
	// }

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		

		
		counter = 0;
		last = 11;
		start = (Button) findViewById(R.id.bStart);
		back = (Button) findViewById(R.id.bBack);
		forw = (Button) findViewById(R.id.bForw);
		exit = (Button) findViewById(R.id.bExit);
		if (counter < 1){
			back.setVisibility(View.INVISIBLE);
			forw.setVisibility(View.INVISIBLE);
			start.setVisibility(View.VISIBLE);}
		//piirra(counter);
		

		TextView tv1 = (TextView) this.findViewById(R.id.TextView03);
		tv1.setSelected(true); // Set focus to the textview

		TextView tv2 = (TextView) this.findViewById(R.id.TextView04);
		tv2.setSelected(true); // Set focus to the textview

		kuva = (ImageView) findViewById(R.id.kuva);
		//timosuite.setImageResource(R.drawable.timo_hello);
		//bubblerightimage = (ImageView) findViewById(R.id.bubbleRight);

		start.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {
				counter++;
				piirra(counter);
				forw.setVisibility(View.VISIBLE);
				start.setVisibility(View.INVISIBLE);
				back.setVisibility(View.INVISIBLE);


			}
		});
		
		forw.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {
				counter++;
				piirra(counter);
				back.setVisibility(View.VISIBLE);
				start.setVisibility(View.INVISIBLE);


			}
		});

		back.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {
				counter--;
				back.setVisibility(View.VISIBLE);
				start.setVisibility(View.INVISIBLE);
				if (counter < 2){
					back.setVisibility(View.INVISIBLE);
					counter = 1;}
				piirra(counter);
				forw.setVisibility(View.VISIBLE);

			}
		});
		
		exit.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {
				finish();
			}
		});


		// timosuite.setImageResource(R.drawable.timo_suite);

		/**
		 * Runnable r = new Runnable() {
		 * 
		 * @Override public void run(){ // doSomething(); //<-- put your code in
		 *           here. timosuite.setImageBitmap(null);
		 *           timosuite.setImageResource(R.drawable.timo_suite); } };
		 * 
		 *           Handler h = new Handler(); h.postDelayed(r, 1000); // <--
		 *           the "1000" is the delay time in miliseconds.
		 */

		/**
		 * viive(1000, 1); SystemClock.sleep(1000); viive(1000, 2);
		 * SystemClock.sleep(1000); viive(1000, 1); SystemClock.sleep(1000);
		 * viive(1000, 2);
		 */

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	public void piirra(int counter2) {
		LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) kuva
				.getLayoutParams();

		switch (counter2) {

		case 1:
			//params.gravity = Gravity.LEFT;
			kuva.setImageResource(R.drawable.k1);
			//timosuite.setImageResource(R.drawable.timo_hello);
			MediaPlayer mp = MediaPlayer.create(this, R.raw.s1);
			mp.start();

			
			break;
		case 2:
			kuva.setImageResource(R.drawable.k2);
			//params.gravity = Gravity.RIGHT;
			//erightimage.setImageResource(R.drawable.letmeintroduce);
			//timosuite.setImageResource(R.drawable.timo_certificate);
			MediaPlayer mp2 = MediaPlayer.create(this, R.raw.s2);
			mp2.start();
			break;
		case 3:
			kuva.setImageResource(R.drawable.k3);
			//timosuite.setImageResource(R.drawable.suite_handup_transparent);
			MediaPlayer mp3 = MediaPlayer.create(this, R.raw.s3);
			mp3.start();
			break;
		case 4:
			kuva.setImageResource(R.drawable.k4);
			//timosuite.setImageResource(R.drawable.timo_suite);
			MediaPlayer mp4 = MediaPlayer.create(this, R.raw.s4);
			mp4.start();
			break;
		case 5:
			kuva.setImageResource(R.drawable.k5);
			//timosuite.setImageResource(R.drawable.suite_handup_transparent);
			MediaPlayer mp5 = MediaPlayer.create(this, R.raw.s5);
			mp5.start();
			break;
		case 6:
			kuva.setImageResource(R.drawable.k6);
			MediaPlayer mp6 = MediaPlayer.create(this, R.raw.s6);
			mp6.start();
			//timosuite.setImageResource(R.drawable.suite_handup_transparent);
			break;
		case 7:
			kuva.setImageResource(R.drawable.k7);
			MediaPlayer mp7 = MediaPlayer.create(this, R.raw.s7);
			mp7.start();
			//timosuite.setImageResource(R.drawable.suite_handup_transparent);
			break;
		case 8:
			kuva.setImageResource(R.drawable.k8);
			MediaPlayer mp8 = MediaPlayer.create(this, R.raw.s8);
			mp8.start();
			//timosuite.setImageResource(R.drawable.suite_handup_transparent);
			break;
		case 9:
			kuva.setImageResource(R.drawable.k9);
			MediaPlayer mp9 = MediaPlayer.create(this, R.raw.s9);
			mp9.start();
			//timosuite.setImageResource(R.drawable.suite_handup_transparent);
			break;
		case 10:
			kuva.setImageResource(R.drawable.k10);
			MediaPlayer mp10 = MediaPlayer.create(this, R.raw.s10);
			mp10.start();
			//timosuite.setImageResource(R.drawable.suite_handup_transparent);
			break;
		case 11:
			kuva.setImageResource(R.drawable.k11);
			MediaPlayer mp11 = MediaPlayer.create(this, R.raw.s11);
			mp11.start();
			forw.setVisibility(View.INVISIBLE);
			//timosuite.setImageResource(R.drawable.suite_handup_transparent);
			break;
		}
	}

	/**
	 * public void viive(int time, int i){ //Inserting delay here try {
	 * Thread.sleep(time); if (i == 1 ) { timosuite.setImageBitmap(null);
	 * timosuite.setImageResource(R.drawable.timo_suite); } if (i == 2 ) {
	 * bubblerightimage.setImageBitmap(null);
	 * bubblerightimage.setImageResource(R.drawable.letmeintroduce); } } catch
	 * (InterruptedException e) { e.printStackTrace(); } }
	 */

}
